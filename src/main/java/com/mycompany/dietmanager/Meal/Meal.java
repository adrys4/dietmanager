/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dietmanager.Meal;

import com.mycompany.dietmanager.Nutritional.NutritionValue;
import com.mycompany.dietmanager.XML.XMLManager;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 * This Class represent all functionality with Create/Update/Add Meal table.
 * @author adrys
 */
public class Meal {
    
    private final String name;
    private final float protein;
    private final float carbo;
    private final float fat;
    private int weight = 100;
    private int meal = 1;
    private float kcal;
    XMLManager xmlManager;

    public Meal(int meal, String name, float protein, float carbo, float fat, int weight) {
        this.name = name;
        this.protein = protein;
        this.carbo = carbo;
        this.fat = fat;
        this.weight = weight;
        this.meal = meal;
    }
    
    public Meal(int meal, String name, float protein, float carbo, float fat, int weight, float kcal) {
        this.name = name;
        this.protein = protein;
        this.carbo = carbo;
        this.fat = fat;
        this.weight = weight;
        this.meal = meal;
        this.kcal = kcal;
    }
        
    /**
    * This method represent add to Row new partial Meal
    * @author adrys
    * @param jTable
    * @return Meal Object
    */
    public Meal addDataRow(JTable jTable) {    
        kcal = (float)NutritionValue.protein * protein + (float)NutritionValue.carbo * carbo + (float)NutritionValue.fat * fat;
        Object[] row = { meal,
                         name,
                         protein, 
                         carbo, 
                         fat,
                         weight,
                         kcal };    
        DefaultTableModel model = (DefaultTableModel) jTable.getModel();
        model.addRow(row);
        return new Meal(meal, name, protein, carbo, fat, weight);
    }
    
    /**
    * Update XML File
    * @author adrys
    * @param jTable
    * @param file
    * @return Meal Object
    */
    public Meal addDataRowToXml(JTable jTable, String file){
        xmlManager = new XMLManager(file);
        xmlManager.saveProductToXML(jTable);
        return this;
    }
    
    /**
    * Get Sum value from column in JTable
    * @author adrys
    * @param jTable
    * @param column
    * @return total value from column
    */
    public static float getSum(JTable jTable, int column){
        float sum = 0;
        for (int i = 0; i < jTable.getRowCount(); i ++) {
            sum = sum + Float.parseFloat(jTable.getValueAt(i, column).toString());
        }
        return sum;
    }
}
