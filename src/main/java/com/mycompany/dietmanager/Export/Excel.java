/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dietmanager.Export;

import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JFileChooser;
import javax.swing.JTable;
import javax.swing.table.TableModel;

/**
 * This class represent methods for export xlsx file from JTable.
 * @author adrys
 */
public class Excel {
    
    public static void toExcel(JTable table) {
        try {
            JFileChooser fileChooser = new JFileChooser();
            FileWriter excel;
            if (fileChooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {      
                TableModel model = table.getModel();
                excel = new FileWriter(fileChooser.getSelectedFile() + ".xls");
                for(int i = 0; i < model.getColumnCount(); i++){
                    excel.write(model.getColumnName(i) + "\t");
                }
                excel.write("\n");
                for(int i=0; i< model.getRowCount(); i++) {
                    for(int j=0; j < model.getColumnCount(); j++) {
                        excel.write(model.getValueAt(i,j).toString()+"\t");
                    }
                    excel.write("\n");
                }
                excel.close();
            }
        }
        catch(IOException e){ 
            System.out.println(e); 
        }
    }
}