/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dietmanager.Product;

import com.mycompany.dietmanager.Nutritional.NutritionValue;
import com.mycompany.dietmanager.XML.XMLManager;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 * This class represent method for adding / update data with product list
 * @author adrys
 */
public class Product {
    
    private final String name;
    private final float protein;
    private final float carbo;
    private final float fat;
    private final float kcal;
    private XMLManager xmlManager;
    
    public Product(String name, float protein, float carbo, float fat) {
        this.name = name;
        this.protein = protein;
        this.carbo = carbo;
        this.fat = fat;
        kcal = (float)NutritionValue.protein * protein + (float)NutritionValue.carbo * carbo + (float)NutritionValue.fat * fat;
    }
    
    /**
    * add to Row in JTable new Product
    * @author adrys
    * @param jTable
    * @return Product
    */
    public Product addDataRow(JTable jTable) {          

        Object[] row = { name,
                         protein, 
                         carbo, 
                         fat,
                         kcal };    
     
        DefaultTableModel model = (DefaultTableModel) jTable.getModel();
        model.addRow(row);
        return new Product(name, protein, carbo, fat);
    }
    
    /**
    * Update XML 
    * @author adrys
    * @param jTable
    * @param file
    * @return Product
    */
    public Product addDataRowToXml(JTable jTable, String file){
        xmlManager = new XMLManager(file);
        xmlManager.saveProductToXML(jTable);    
        return this;
    }
}