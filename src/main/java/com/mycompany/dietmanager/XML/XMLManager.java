/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dietmanager.XML;
import com.mycompany.dietmanager.Meal.Meal;
import com.mycompany.dietmanager.Product.Product;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComboBox;
import static javax.swing.JOptionPane.showMessageDialog;
import javax.swing.JTable;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

/**
 * This class represents methods for create/update/delete Products or Meals to XML File
 * @author adrys
 */
public class XMLManager {
    
    String file;
    Product product;
    Meal meal;
    
    public XMLManager(String file) {
        this.file = file;
    }
    
    public void saveProducts(String[] name, String[] protein, String[] carbo, String[] fat){
        try {
           DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
           DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

           // root elements
           Document doc = docBuilder.newDocument();
           Element rootElement = doc.createElement("Products");
           doc.appendChild(rootElement);

           for (int j = 1; j < name.length + 1; j ++) {
           // staff elements
           Element staff = doc.createElement("Product");
           rootElement.appendChild(staff);

           // set attribute to staff element
           Attr attr = doc.createAttribute("id");
           attr.setValue(Integer.toString(j));
           staff.setAttributeNode(attr);

           // shorten way
           // staff.setAttribute("id", "1");

           // productname elements
           Element productName = doc.createElement("name");
           productName.appendChild(doc.createTextNode(name[j-1]));
           staff.appendChild(productName);

           // protein element
           Element proteinValue = doc.createElement("protein");
           proteinValue.appendChild(doc.createTextNode(protein[j-1]));
           staff.appendChild(proteinValue);

           // carbo element
           Element carboValue = doc.createElement("carbo");
           carboValue.appendChild(doc.createTextNode(carbo[j-1]));
           staff.appendChild(carboValue);

           // fat element
           Element fatValue = doc.createElement("fat");
           fatValue.appendChild(doc.createTextNode(fat[j-1]));
           staff.appendChild(fatValue);
           }
           // write the content into xml file
           TransformerFactory transformerFactory = TransformerFactory.newInstance();
           Transformer transformer = transformerFactory.newTransformer();
           transformer.setOutputProperty(OutputKeys.INDENT, "yes");
           transformer.setOutputProperty(OutputKeys.METHOD, "xml");
           transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

           DOMSource source = new DOMSource(doc);
           StreamResult result = new StreamResult(new File(file));

           transformer.transform(source, result);
           } 
           catch (ParserConfigurationException | TransformerException pce) {
               showMessageDialog(null, "Error: " + pce.toString());
           }
        }
    
    public void saveMeals(String[] meal, String[] name, String[] protein, String[] carbo, String[] fat, String[] weight, String[] kcal){
        try {
           DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
           DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

           // root elements
           Document doc = docBuilder.newDocument();
           Element rootElement = doc.createElement("Products");
           doc.appendChild(rootElement);

           for (int j = 1; j < name.length + 1; j ++) {
           // staff elements
           Element staff = doc.createElement("Product");
           rootElement.appendChild(staff);

           // set attribute to staff element
           Attr attr = doc.createAttribute("id");
           attr.setValue(Integer.toString(j));
           staff.setAttributeNode(attr);

           // productname elements

           // meal element
           Element mealValue = doc.createElement("meal");
           mealValue.appendChild(doc.createTextNode(meal[j-1]));
           staff.appendChild(mealValue);

           Element productName = doc.createElement("name");
           productName.appendChild(doc.createTextNode(name[j-1]));
           staff.appendChild(productName);

           // protein element
           Element proteinValue = doc.createElement("protein");
           proteinValue.appendChild(doc.createTextNode(protein[j-1]));
           staff.appendChild(proteinValue);

           // carbo element
           Element carboValue = doc.createElement("carbo");
           carboValue.appendChild(doc.createTextNode(carbo[j-1]));
           staff.appendChild(carboValue);

           // fat element
           Element fatValue = doc.createElement("fat");
           fatValue.appendChild(doc.createTextNode(fat[j-1]));
           staff.appendChild(fatValue);

           // weight element
           Element weightValue = doc.createElement("weight");
           weightValue.appendChild(doc.createTextNode(weight[j-1]));
           staff.appendChild(weightValue);

           // kcal element
           Element kcalValue = doc.createElement("kcal");
           kcalValue.appendChild(doc.createTextNode(kcal[j-1]));
           staff.appendChild(kcalValue);
           }
           // write the content into xml file
           TransformerFactory transformerFactory = TransformerFactory.newInstance();
           Transformer transformer = transformerFactory.newTransformer();
           transformer.setOutputProperty(OutputKeys.INDENT, "yes");
           transformer.setOutputProperty(OutputKeys.METHOD, "xml");
           transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

           DOMSource source = new DOMSource(doc);
           StreamResult result = new StreamResult(new File(file));

           // Output to console for testing
           // StreamResult result = new StreamResult(System.out);

           transformer.transform(source, result);
           } catch (ParserConfigurationException | TransformerException pce) {
               showMessageDialog(null, "Error: " + pce.toString());
           }
    }
    
    public void loadProductsList(JTable jTable) throws ParserConfigurationException, SAXException, IOException { 
        Document document = getDocument(file);
        int count = document.getElementsByTagName("name").getLength();     
        for (int i = 0; i < count; i++){
            String nm = document.getElementsByTagName("name").item(i).getTextContent();
            float pro = Float.parseFloat(document.getElementsByTagName("protein").item(i).getTextContent());
            float carb = Float.parseFloat(document.getElementsByTagName("carbo").item(i).getTextContent());
            float fat = Float.parseFloat(document.getElementsByTagName("fat").item(i).getTextContent());
            product = new Product(nm,pro,carb,fat).addDataRow(jTable);
        }
    }
    
   public void loadMealList(JTable jTable) throws ParserConfigurationException, SAXException, IOException {     
        Document document = getDocument(file);
        int count = document.getElementsByTagName("name").getLength();
        for (int i = 0; i < count; i++){
            int meall = Integer.parseInt(document.getElementsByTagName("meal").item(i).getTextContent());
            String nm = document.getElementsByTagName("name").item(i).getTextContent();
            float pro = Float.parseFloat(document.getElementsByTagName("protein").item(i).getTextContent());
            float carb = Float.parseFloat(document.getElementsByTagName("carbo").item(i).getTextContent());
            float fat = Float.parseFloat(document.getElementsByTagName("fat").item(i).getTextContent());
            int weight = Integer.parseInt(document.getElementsByTagName("weight").item(i).getTextContent());
            float kcal = Float.parseFloat(document.getElementsByTagName("kcal").item(i).getTextContent());
            meal = new Meal(meall, nm,pro,carb,fat,weight,kcal).addDataRow(jTable);
        }
    }
        
    public void loadProductsListByName(JTable jTable, String name, int mealNumber, int weight) throws ParserConfigurationException, SAXException, IOException {     
        Document document = getDocument(file);     
        int i = getIndexByName(name);       
        if (i != -1) {
            float multiplier = weight / 100f;
            System.out.println("multiplier: " + multiplier);
            String nm = document.getElementsByTagName("name").item(i).getTextContent();            
            float pro = Float.parseFloat(document.getElementsByTagName("protein").item(i).getTextContent()) * multiplier;
            float carb = Float.parseFloat(document.getElementsByTagName("carbo").item(i).getTextContent()) * multiplier;
            float fat = Float.parseFloat(document.getElementsByTagName("fat").item(i).getTextContent()) * multiplier;
            meal = new Meal(mealNumber,nm,pro,carb,fat,weight).addDataRow(jTable);
        }
    }
        
    public void addProductNameToComboBox(JComboBox jComboBox) throws SAXException, IOException, ParserConfigurationException{
        List<String> list;
        list = getProductsListName();
        for (int i = 0; i < list.size(); i++){
            jComboBox.addItem(list.get(i));
        }
    }
    
    private List<String> getProductsListName() throws SAXException, IOException, ParserConfigurationException{
        List<String> list; 
        list = new ArrayList<>();       
        Document document = getDocument(file);
        int count = document.getElementsByTagName("name").getLength();      
        for (int i = 0; i < count; i++){
            list.add(document.getElementsByTagName("name").item(i).getTextContent());
        }
        return list;      
    }
    
    public void saveProductToXML(JTable jTable){
        int lenght = getLenghtProduct(jTable);
        String[] names, proteins, carbos, fats;
        names = new String[lenght];
        proteins = new String[lenght];
        carbos = new String[lenght];
        fats = new String[lenght];
        //get all components  
        for (int p = 0; p < lenght; p++) {
            names[p] = jTable.getValueAt(p, 0).toString();
        }  
        for (int p = 0; p < lenght; p++) {
            proteins[p] = jTable.getValueAt(p, 1).toString();
        }
        for (int p = 0; p < lenght; p++) {
            carbos[p] =  jTable.getValueAt(p, 2).toString();     
        }
        for (int p = 0; p < lenght; p++) {
            fats[p] = jTable.getValueAt(p, 3).toString();           
        }
        saveProducts(names, proteins, carbos, fats);        
    }
    
    public void saveMealToXML(JTable jTable){
        int lenght = getLenghtProduct(jTable);
        String[] proteins, names, carbos, fats, weights, meals, kcal;
        names = new String[lenght];
        proteins = new String[lenght];
        carbos = new String[lenght];
        fats = new String[lenght];
        meals = new String[lenght];
        weights = new String[lenght];
        kcal = new String[lenght];
        
        //get all components  
        for (int p = 0; p < lenght; p++) {
            meals[p] = jTable.getValueAt(p, 0).toString();
        }  
        for (int p = 0; p < lenght; p++) {
            names[p] = jTable.getValueAt(p, 1).toString();
        }  
        for (int p = 0; p < lenght; p++) {
            proteins[p] = jTable.getValueAt(p, 2).toString();
        }
        for (int p = 0; p < lenght; p++) {
            carbos[p] =  jTable.getValueAt(p, 3).toString();     
        }
        for (int p = 0; p < lenght; p++) {
            fats[p] = jTable.getValueAt(p, 4).toString();           
        }
        for (int p = 0; p < lenght; p++) {
            weights[p] = jTable.getValueAt(p, 5).toString();           
        }
        for (int p = 0; p < lenght; p++) {
            kcal[p] = jTable.getValueAt(p, 6).toString();           
        }
        saveMeals(meals, names, proteins, carbos, fats, weights, kcal);        
    }
        
    private int getLenghtProduct(JTable jTable){
        return jTable.getRowCount();
    }
    
    private int getIndexByName(String name) throws SAXException, ParserConfigurationException, IOException{
        List<String> list; 
        list = new ArrayList<>();       
        Document document = getDocument(file);
        int count = document.getElementsByTagName("name").getLength();        
        for (int i = 0; i < count; i++){
            list.add(document.getElementsByTagName("name").item(i).getTextContent());
            if (document.getElementsByTagName("name").item(i).getTextContent().contains(name)){
                return i;
            }
        }       
        return -1;     
    }  
    
    private Document getDocument(String file) throws ParserConfigurationException, SAXException, IOException{
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
        .newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse(new File(file));
        return document;
    }
}